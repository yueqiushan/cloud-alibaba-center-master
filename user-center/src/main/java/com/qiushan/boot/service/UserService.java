package com.qiushan.boot.service;

import com.qiushan.boot.domain.entity.bonusEventLog.BonusEventLog;
import com.qiushan.boot.domain.entity.user.User;
import com.qiushan.boot.dto.UserAddBonusMsgDTO;
import com.qiushan.boot.mapper.bonusEventLog.BonusEventLogMapper;
import com.qiushan.boot.mapper.user.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Author yuedeliang
 * @Date 2019年11月18日 10:32:54
 */
@Slf4j
@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    BonusEventLogMapper bonusEventLogMapper;

    @Transactional(rollbackFor = Exception.class)
    public void addBonus(UserAddBonusMsgDTO msgDTO) {
        // 1. 为用户加积分
        Integer userId = msgDTO.getUserId();
        Integer bonus = msgDTO.getBonus();
        User user = this.userMapper.selectByPrimaryKey(userId);

        user.setBonus(user.getBonus() + bonus);
        this.userMapper.updateByPrimaryKeySelective(user);

        // 2. 记录日志到bonus_event_log表里面
        this.bonusEventLogMapper.insert(
                BonusEventLog.builder()
                        .userId(userId)
                        .value(bonus)
                        .event("CONTRIBUTE")
                        .createTime(new Date())
                        .description("投稿加积分..")
                        .build()
        );
        log.info("积分添加完毕...");
    }

    public User findById(Integer id){
        log.info("==== 请求id：{}", id);
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }
}
