package com.qiushan.boot.controller;

import com.qiushan.boot.domain.entity.user.User;
import com.qiushan.boot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yuedeliang
 * @Date 2019年11月18日 10:34:05
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/{id}")
    public User findById(@PathVariable Integer id){
        User byId = userService.findById(id);
        return byId;
    }

    @GetMapping("/id")
    public User findById2(Integer id){
        User byId = userService.findById(id);
        return byId;
    }
}
