package com.qiushan.boot.controller;

import com.qiushan.boot.domain.entity.user.User;
import com.qiushan.boot.mapper.user.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Optional;

/**
 * @Author yuedeliang
 * @Date 2019年10月17日 13:47:49
 */
@Slf4j
@RestController
@RequestMapping("/test/")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestController {


    @GetMapping("xxx")
    public User testUser(){


        return null;

    }

    final UserMapper userMapper;

    @GetMapping("user")
    public User userTest() {
        User user = new User();
        user.setCreateTime(new Date());
        user.setWxId("222222");
        user.setWxNickname("qiushan222");
        user.setBonus(100);
        user.setUpdateTime(new Date());
        userMapper.insertSelective(user);
        User user1 = userMapper.selectById(user.getId());
        log.info(user1.toString());
        User user2 = userMapper.selectByPrimaryKey(user.getId());
        log.info(user2.toString());
        return user;
    }

    @GetMapping("/queryUser")
    public User queryUser(User user){
        log.info("query - userDTO:{}",user);
        User user1 = userMapper.selectByPrimaryKey(user.getId());
        return user1;
    }
}
