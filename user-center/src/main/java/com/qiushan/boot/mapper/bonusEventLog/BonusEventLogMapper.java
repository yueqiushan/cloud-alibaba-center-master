package com.qiushan.boot.mapper.bonusEventLog;

import com.qiushan.boot.domain.entity.bonusEventLog.BonusEventLog;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface BonusEventLogMapper extends Mapper<BonusEventLog> {
}