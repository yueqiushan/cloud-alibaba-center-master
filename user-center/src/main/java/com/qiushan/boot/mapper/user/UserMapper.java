package com.qiushan.boot.mapper.user;

import com.qiushan.boot.domain.entity.user.User;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface UserMapper extends Mapper<User> {

    @Select("select * from user where id = #{id}")
    User selectById(Integer id);


}