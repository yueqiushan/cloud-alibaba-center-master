package com.qiushan.boot.rocketmq;

import com.qiushan.boot.dto.UserAddBonusMsgDTO;
import com.qiushan.boot.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.stereotype.Service;

/**
 * @Author yueqiushan
 * @Date 2019年11月27日 21:29:28
 */
@Slf4j
@Service
public class StreamBinderConsumerService {

    @Autowired
    UserService userService;

    @StreamListener(Sink.INPUT)
    public void receive(UserAddBonusMsgDTO userAddBonusMsgDTO){
        log.info("=== 通过stream接受到消息体：{}", userAddBonusMsgDTO);
        this.userService.addBonus(userAddBonusMsgDTO);
    }

    /**
     * 全局异常处理
     *
     * @param message 发生异常的消息
     */
    @StreamListener("errorChannel")
    public void error(Message<?> message) {
        /**
         * 全局异常处理是消费者处理消息过程中发生异常，进行的回调处理
         * 如果发生异常，MQ会循环重试3次，最后还是失败就会打印异常，重试成功的话正常处理返回成功，
         * 重试的过程本身就是消息的重发，所以要做好消息去重的处理。
         *
         * 首次循环3次异常，MQ之后还会定时重试几十次;
         */
        ErrorMessage errorMessage = (ErrorMessage) message;
        log.warn("发生异常, errorMessage = {}", errorMessage);
    }
}
