package com.qiushan.boot.rocketmq;

import com.qiushan.boot.domain.entity.bonusEventLog.BonusEventLog;
import com.qiushan.boot.domain.entity.user.User;
import com.qiushan.boot.dto.UserAddBonusMsgDTO;
import com.qiushan.boot.mapper.bonusEventLog.BonusEventLogMapper;
import com.qiushan.boot.mapper.user.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 积分消费者监听
 *
 * @author yuedeliang
 * @date 2019-11-25 17:34:58
 */
@Slf4j
@Service
// 整合stream后，rocket配置属性被移除，这里不能生效
//@RocketMQMessageListener(consumerGroup = "user-center-group", topic = "add-bonus")
public class AddBonusListener implements RocketMQListener<UserAddBonusMsgDTO> {

    @Autowired
    UserMapper userMapper;

    @Autowired
    BonusEventLogMapper bonusEventLogMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void onMessage(UserAddBonusMsgDTO userAddBonusMsgDTO) {
        // 1.加积分
        log.info("=== userAddBonusMsgDTO:{}",userAddBonusMsgDTO);

        User user = userMapper.selectByPrimaryKey(userAddBonusMsgDTO.getUserId());
        if (user == null){
            return;
        }
        user.setBonus(user.getBonus() + userAddBonusMsgDTO.getBonus());
        userMapper.updateByPrimaryKeySelective(user);
        // 2.添加日志

        BonusEventLog bonusEventLog = new BonusEventLog();
        bonusEventLog.setUserId(user.getId());
        bonusEventLog.setValue(userAddBonusMsgDTO.getBonus());
        bonusEventLog.setEvent("add-bonus");
        bonusEventLog.setDescription("投稿加积分活动");
        bonusEventLog.setCreateTime(new Date());
        bonusEventLogMapper.insert(bonusEventLog);

        log.info("积分添加完毕...");
    }
}
