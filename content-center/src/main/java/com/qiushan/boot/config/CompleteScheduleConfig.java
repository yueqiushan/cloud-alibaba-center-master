package com.qiushan.boot.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDateTime;

/**
 * @Author yuedeliang
 * @Date 2019年09月18日 17:11:46
 */
@Slf4j
//@Configuration
//@EnableScheduling
public class CompleteScheduleConfig implements SchedulingConfigurer {


    /**
     * 执行定时任务.
     */
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(
                //1.添加任务内容(Runnable)
                () -> {

                    System.out.println(Thread.currentThread().getName()+"========= 执行定时任务: " + LocalDateTime.now().toLocalTime());
                },
                //2.设置执行周期(Trigger)
                triggerContext -> {
                    //2.1 从数据库获取执行周期
                    String cron = null;
                    log.info("=============== cron: "+cron);
                    cron = "0 * * * * ?";
                 //2.3 返回执行周期(Date)
                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
                }
        );
    }

}
