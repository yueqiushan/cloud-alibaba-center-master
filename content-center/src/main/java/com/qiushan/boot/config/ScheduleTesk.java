package com.qiushan.boot.config;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Author yuedeliang
 * @Date 2019年09月19日 15:52:38
 */
@Component
public class ScheduleTesk {

    //@Scheduled(cron = "${schedule.tesk-cron}")
    public void tesk1(){
        System.out.println(Thread.currentThread().getName()+"========= 执行定时任务tesk1: " + LocalDateTime.now().toLocalTime());
    }

//    @Async
//    @Scheduled(cron = "0 0/2 * * * ?")
    public void tesk2(){
        System.out.println(Thread.currentThread().getName()+"========= 执行定时任务tesk2: " + LocalDateTime.now().toLocalTime());
    }
}
