package com.qiushan.boot.config;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContextAware;

/**
 * @Author yuedeliang
 * @Date 2019年09月03日 10:01:04
 */
public abstract class TestSpringContext implements ApplicationContextAware, DisposableBean {

}
