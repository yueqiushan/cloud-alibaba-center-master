package com.qiushan.boot.mapper.messagelog;

import com.qiushan.boot.domain.entity.messagelog.RocketmqTransactionLog;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface RocketmqTransactionLogMapper extends Mapper<RocketmqTransactionLog> {
}