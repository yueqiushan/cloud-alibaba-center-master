package com.qiushan.boot.mapper.notice;

import com.qiushan.boot.domain.entity.notice.Notice;
import tk.mybatis.mapper.common.Mapper;

public interface NoticeMapper extends Mapper<Notice> {
}