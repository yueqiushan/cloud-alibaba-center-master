package com.qiushan.boot.mapper.midUserShare;

import com.qiushan.boot.domain.entity.midUserShare.MidUserShare;
import tk.mybatis.mapper.common.Mapper;

public interface MidUserShareMapper extends Mapper<MidUserShare> {
}