package com.qiushan.boot.mapper.share;

import com.qiushan.boot.domain.entity.share.Share;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface ShareMapper extends Mapper<Share> {
}