package com.qiushan.boot.rocketmq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 *  自定义的stream的生产者Source
 *
 * @Author yueqiushan
 * @Date 2019年11月27日 21:51:33
 */
public interface MySource {


    String MY_OUTPUT = "my-output";

    @Output(MY_OUTPUT)
    MessageChannel output();
}
