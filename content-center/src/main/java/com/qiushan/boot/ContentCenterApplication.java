package com.qiushan.boot;

import com.qiushan.boot.rocketmq.MySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * Mapper包扫描时一定要注意路径精确, 否则会影响其他包下的接口被Mapper包装
 *(basePackages={"com.qiushan.boot.feignclient"})
 * @author yuedeliang
 */
@MapperScan("com.qiushan.boot.mapper")
@SpringBootApplication
@EnableFeignClients
//@EnableBinding(Source.class)
@EnableBinding({Source.class, MySource.class})
public class ContentCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContentCenterApplication.class, args);
    }

}
