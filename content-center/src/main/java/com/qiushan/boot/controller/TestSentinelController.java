package com.qiushan.boot.controller;

import com.qiushan.boot.service.TestSentinelService;
import com.qiushan.boot.service.impl.TestSentinelServiceImpl2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yueqiushan
 * @Date 2019年11月30日 15:52:52
 */
@Slf4j
@RestController
public class TestSentinelController {


    @Autowired
    TestSentinelService sentinelService;

    @GetMapping("sentinel-a")
    public String testA(){
        log.info(" === test a ... ");
        sentinelService.common();
        return "sentinel-a";
    }

    @GetMapping("sentinel-b")
    public String testB(){
        log.info(" === test b ... ");
        sentinelService.common();
        return "sentinel-b";
    }

    @Autowired
    TestSentinelServiceImpl2 sentinelService2;
    @GetMapping("test-a")
    public String testC(){
        log.info(" === test A ... ");
        sentinelService2.common();
        return "sentinel-A";
    }

    @GetMapping("test-b")
    public String testD(){
        log.info(" === test B ... ");
        sentinelService2.common();
        return "sentinel-B";
    }


}
