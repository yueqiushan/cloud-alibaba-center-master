package com.qiushan.boot.controller;

import com.qiushan.boot.domain.entity.share.Share;
import com.qiushan.boot.dto.UserDTO;
import com.qiushan.boot.feignclient.TestUserCenterFeignClient;
import com.qiushan.boot.mapper.share.ShareMapper;
import com.qiushan.boot.rocketmq.MySource;
import com.qiushan.boot.service.ShareService;
import com.sun.media.jfxmedia.logging.Logger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @Author yuedeliang
 * @Date 2019年10月17日 13:47:49
 */
@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestController {

    final ShareMapper shareMapper;

    @Autowired
    ShareService shareService;

    @GetMapping("share")
    public Share testUser(){
        Share share = new Share();
        share.setAuditStatus("1");
        share.setAuthor("qiu");
        share.setCover("cover");
        share.setPrice(111);
        share.setCreateTime(new Date());
        share.setUpdateTime(new Date());

        Share build = Share.builder()
                .auditStatus("")
                .author("")
                .buyCount(1)
                .build();
        shareMapper.insertSelective(share);

        Share result = shareMapper.selectByPrimaryKey(share.getId());
        return result;

    }

    @GetMapping("all")
    public List<Share> test(){
        List<Share> lists = shareMapper.selectAll();
        return lists;
    }

    /**
     * nacos 的发现客户端
     */
    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("discovery")
    public List<ServiceInstance> getDiscovery(String name){
        if (StringUtils.isEmpty(name)){
            name = "content-center";
        }
        // 服务发现组件，可以沟通组件获取注册的服务的信息; 其他的组件eureka/zookeeper/consul 等都是可以用这个discoveryClient组件
        List<ServiceInstance> instances = discoveryClient.getInstances(name);
        return instances;
    }

    @Autowired
    private TestUserCenterFeignClient testUserCenterFeignClient;

    @GetMapping("test-get")
    public UserDTO query(UserDTO userDTO) {
        log.info("=== query user entity param:{}",userDTO);
        return testUserCenterFeignClient.query(userDTO);
    }

    @Autowired
    private MySource mySource;

    @GetMapping("/mysource-stream")
    public String myStream() {

        this.mySource.output()
                .send(
                        MessageBuilder
                                .withPayload("这是一个mysource - stream的消息体")
                                .build()
                );
        return "success";
    }

}
