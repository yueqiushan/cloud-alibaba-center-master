package com.qiushan.boot.controller;

import com.qiushan.boot.domain.entity.share.Share;
import com.qiushan.boot.service.ShareService;
import com.qiushan.boot.vo.ShareVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yuedeliang
 * @Date 2019年11月18日 14:43:59
 */
@RestController
@RequestMapping("/share/")
public class ShareController {

    @Autowired
    ShareService shareService;

    @GetMapping("/{id}")
    public ShareVO getById(@PathVariable Integer id){
        ShareVO shareById = shareService.getShareById(id);
        return shareById;
    }



}
