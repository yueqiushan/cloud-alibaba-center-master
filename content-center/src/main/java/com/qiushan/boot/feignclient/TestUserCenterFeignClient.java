package com.qiushan.boot.feignclient;

import com.qiushan.boot.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "user-center")
public interface TestUserCenterFeignClient {

    /**
     *  远程调用user服务接口
     *  对象参数 get 请求多字段参数 @SpringQueryMap否则为null
     * @param userDTO
     * @return
     */
    @GetMapping("/test/queryUser")
    UserDTO query(@SpringQueryMap UserDTO userDTO);
}
