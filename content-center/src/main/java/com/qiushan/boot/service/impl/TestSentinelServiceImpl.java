package com.qiushan.boot.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.qiushan.boot.service.TestSentinelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Author yueqiushan
 * @Date 2019年11月30日 15:54:54
 */
@Slf4j
@Service
public class TestSentinelServiceImpl implements TestSentinelService {

    @Override
    @SentinelResource("common")
    public String common() {
        log.info(" === common run ...");
        return " common run ...";
    }
}
