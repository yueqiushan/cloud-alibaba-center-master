package com.qiushan.boot.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiushan.boot.domain.entity.messagelog.RocketmqTransactionLog;
import com.qiushan.boot.domain.entity.share.Share;
import com.qiushan.boot.domain.enums.AuditStatusEnum;
import com.qiushan.boot.dto.ShareAuditDTO;
import com.qiushan.boot.dto.UserAddBonusMsgDTO;
import com.qiushan.boot.dto.UserDTO;
import com.qiushan.boot.feignclient.UserCenterFeignClient;
import com.qiushan.boot.mapper.messagelog.RocketmqTransactionLogMapper;
import com.qiushan.boot.mapper.share.ShareMapper;
import com.qiushan.boot.service.ShareService;
import com.qiushan.boot.vo.ShareVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @Author yuedeliang
 * @Date 2019年10月17日 15:34:52
 */
@Slf4j
@Service
public class ShareServiceImpl implements ShareService {

    @Autowired
    ShareMapper shareMapper;

    @Autowired
    RestTemplate restTemplate;

//    @Autowired
//    DiscoveryClient discoveryClient;

    @Autowired
    UserCenterFeignClient userCenterFeignClient;

    @Autowired
    RocketMQTemplate rocketMQTemplate;

    @Autowired
    Source source;

    @Autowired
    RocketmqTransactionLogMapper rocketmqTransactionLogMapper;

    public static final String Add_BONUS_GROUP = "add-bonus-group";
    public static final String TX_Add_BONUS_GROUP = "tx-add-bonus-group";

    public static final String Add_BONUS_TOPIC = "add-bonus";
    public static final String TX_Add_BONUS_TOPIC = "tx-add-bonus";

    @Override
    public ShareVO  getShareById(Integer id){
        Share share = shareMapper.selectByPrimaryKey(id);
        if(share == null){
            return null;
        }
        ShareVO shareVO = new ShareVO();
        BeanUtils.copyProperties(share,shareVO);
        // 熟悉stream --> JDK 8
        // 函数式编程
        //这里的URL替换成服务发现的方式获取
////        List<ServiceInstance> instances = discoveryClient.getInstances("user-center");
////        List<String> targetUrls = instances.stream()
////                .map(instance -> instance.getUri().toString() + "/users/{1}")
//////                .findFirst().orElseThrow(() -> new IllegalArgumentException("当前没有实例"));
////                .collect(Collectors.toList());
////        //随机算法
////        int nextInt = ThreadLocalRandom.current().nextInt(targetUrls.size());
////        log.info("== 服务发现获取到的URL：{}",targetUrls.get(nextInt));
//        UserDTO userDTO = restTemplate.getForObject(targetUrls.get(nextInt), UserDTO.class, share.getUserId());
        Integer userId = share.getUserId();
//        UserDTO userDTO = restTemplate.getForObject("http://user-center/users/id?id={userId}", UserDTO.class, userId);

        UserDTO userDTO = userCenterFeignClient.findById(userId);
        shareVO.setWxId(userDTO.getWxId());
        shareVO.setWxNickname(userDTO.getWxNickname());
        shareVO.setUserId(userDTO.getId());
        shareVO.setRoles(userDTO.getRoles());
        shareVO.setBonus(userDTO.getBonus());

        return shareVO;
    }

    @Override
//    @Transactional(rollbackFor = Exception.class)
    public Share auditById(Integer id, ShareAuditDTO auditDTO) {
        // 1. 查询share是否存在，不存在或者当前的audit_status != NOT_YET，那么抛异常
        Share share = this.shareMapper.selectByPrimaryKey(id);
        if (share == null) {
            throw new IllegalArgumentException("参数非法！该分享不存在！");
        }
        if (!Objects.equals(AuditStatusEnum.NOT_YET.toString(), share.getAuditStatus())) {
            throw new IllegalArgumentException("参数非法！该分享已审核通过或审核不通过！");
        }

        // 3. 如果是PASS，那么发送消息给rocketmq，让用户中心去消费，并为发布人添加积分
        if (AuditStatusEnum.PASS.equals(auditDTO.getAuditStatusEnum())) {
            // 发送半消息。。
            String transactionId = UUID.randomUUID().toString();
            // stream重构事务消息
            this.source.output().send(
                    MessageBuilder
                    .withPayload(
                            UserAddBonusMsgDTO.builder()
                                    .userId(share.getUserId())
                                    .bonus(50)
                                    .sendTime(new Date())
                                    .build()
                                )
                    // header也有妙用...
                    .setHeader(RocketMQHeaders.TRANSACTION_ID, transactionId)
                    .setHeader("share_id", id)
                            .setHeader("dto", JSONObject.toJSONString(auditDTO))
                    .build()
            );
            log.info("==== stream tx 消息发送成功 transactionId：{}", transactionId);
        }
        else {
            this.auditByIdInDB(id, auditDTO);
        }
        return share;
    }


    @Transactional(rollbackFor = Exception.class)
    public void auditByIdInDB(Integer id, ShareAuditDTO auditDTO) {
        Share share = Share.builder()
                .id(id)
                .auditStatus(auditDTO.getAuditStatusEnum().toString())
                .reason(auditDTO.getReason())
                .build();
        this.shareMapper.updateByPrimaryKeySelective(share);

        // 4. 把share写到缓存
    }

    @Override
//    @GlobalTransaction
    @Transactional(rollbackFor = Exception.class)
    public void auditByIdWithRocketMqLog(Integer id, ShareAuditDTO auditDTO, String transactionId) {
        this.auditByIdInDB(id, auditDTO);
        // --> A server   --> B server
        this.rocketmqTransactionLogMapper.insertSelective(
                RocketmqTransactionLog.builder()
                        .transactionId(transactionId)
                        .log("审核分享...")
                        .build()
        );
    }

}
