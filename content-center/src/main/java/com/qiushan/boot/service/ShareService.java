package com.qiushan.boot.service;

import com.qiushan.boot.domain.entity.share.Share;
import com.qiushan.boot.dto.ShareAuditDTO;
import com.qiushan.boot.vo.ShareVO;

/**
 * @Author yuedeliang
 * @Date 2019年10月17日 15:33:56
 */
public interface ShareService {


    /**
     *  获取分享的详情
     *
     * @param id
     * @return
     */
    ShareVO getShareById(Integer id);

    /**
     * 给添加积分
     * @param id
     * @param auditDTO
     * @return
     */
    Share auditById(Integer id, ShareAuditDTO auditDTO);

    /**
     * 本地事务操作
     *
     * @param id
     * @param auditDTO
     * @param transactionId
     */
    void auditByIdWithRocketMqLog(Integer id, ShareAuditDTO auditDTO, String transactionId);
}
